package ru.konekon.vdolgu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

fun Int.toFormattedString() = "%d.%02d".format(this / 100, this % 100)

data class Item(val title: String, val price: Int, val fee: Int)

class ItemAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Item>
) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.list_item, parent, false)
        val idView = rowView.findViewById(R.id.id) as TextView

        val sumView = rowView.findViewById(R.id.sum) as TextView
        val priceView = rowView.findViewById(R.id.price) as TextView
        val feeView = rowView.findViewById(R.id.fee) as TextView

        val titleView = rowView.findViewById(R.id.title) as TextView

        val item = getItem(position) as Item
        idView.text = (position + 1).toString()
        priceView.text = item.price.toFormattedString()
        feeView.text = item.fee.toFormattedString()
        sumView.text = (item.price + item.fee).toFormattedString()
        titleView.text = item.title

        return rowView
    }

    fun addItem(item: Item) {
        dataSource.add(item)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        dataSource.removeAt(position)
        notifyDataSetChanged()
    }

    fun clear() {
        dataSource.clear()
        notifyDataSetChanged()
    }
}
