package ru.konekon.vdolgu

import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import kotlinx.android.synthetic.main.activity_inline_scan.*

class InlineScanActivity : AppCompatActivity() {
    lateinit var captureManager: CaptureManager
    var torchState: Boolean = false
    var currentSum = 0
    lateinit var listAdapter: ItemAdapter

    private fun updateSum(newSum: Int) {
        currentSum += newSum
        txtResult.text = currentSum.toFormattedString()
    }

    private fun add(title: String, sum: Int, fee: Int) {
        listAdapter.addItem(Item(title, sum, fee))
        listView.smoothScrollToPosition(listAdapter.count - 1)
        updateSum(sum + fee)
    }

    private fun removeItem(position: Int) {
        val item = (listAdapter.getItem(position) as Item)
        updateSum(-item.price - item.fee)
        listAdapter.removeItem(position)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inline_scan)

        captureManager = CaptureManager(this, barcodeView)
        captureManager.initializeFromIntent(intent, savedInstanceState)

        btnScan.setOnClickListener {
            barcodeView.decodeSingle(object : BarcodeCallback {
                override fun barcodeResult(result: BarcodeResult?) {
                    result?.let {
                        println(it.text)
                        val vib: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

                        if (vib.hasVibrator()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                vib.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
                            } else {
                                vib.vibrate(200)
                            }
                        }

                        try {
                            val map = it.text.split('|', '&').reversed().associate { splited ->
                                if (splited.count { it == '=' } != 1)
                                    splited to ""
                                else {
                                    val (left, right) = splited.split("=")
                                    left.toLowerCase() to right
                                }
                            }
                            var sum = arrayOf(map["s"], map["sum"], map["count"]).filterNotNull().joinToString("").replace(".", "").toInt()
                            var title = arrayOf(map["purpose"], map["lastname"], map["paymperiod"], map["phone"], map["payeraddress"]).filterNotNull().filter { it.isNotEmpty() }.joinToString(", ")
                            if (title.isEmpty()) title = "Чек"
                            val fees = mapOf(
                                "МАКРОРЕГИОНАЛЬНЫЙ ФИЛИАЛ \"СЕВЕРО-ЗАПАД\" ПАО \"РОСТЕЛЕКОМ\"" to 0.025,
                                "АО «Петроэлектросбыт»" to 0.02,
                                "СПб ГБУ \"КЦСОН Красносельского района\"" to 0.02
                            )
                            val name = map["name"]
                            val fee = if (name in fees) kotlin.math.round(kotlin.math.max(1000.0, fees[name!!]!! * sum)).toInt() else 0
                            add(title, sum, fee)
                        } catch (e: Exception) {
                            println(e)
                            Toast.makeText(applicationContext, it.text, LENGTH_SHORT).show()
                        }
                    }
                }

                override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                }
            })
        }

        btnClear.setOnClickListener {
            updateSum(-currentSum)
            listAdapter.clear()
        }

        btnTorch.setOnClickListener {
            if (torchState) {
                torchState = false
                barcodeView.setTorchOff()
            } else {
                torchState = true
                barcodeView.setTorchOn()
            }
        }

        listAdapter = ItemAdapter(this, arrayListOf())
        listView.adapter = listAdapter
        listView.setOnItemLongClickListener { parent, view, position, id ->
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Are you sure you want to Delete?")
                .setCancelable(true)
                .setPositiveButton("Yes") { _, _ ->
                    removeItem(position)
                }
                .setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }
            builder.create().show()
            true
        }
    }

    override fun onPause() {
        super.onPause()
        captureManager.onPause()
    }

    override fun onResume() {
        super.onResume()
        captureManager.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        captureManager.onDestroy()
    }
}
